package com.androiddemo.wavelet.simpleloginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class CustomerList extends AppCompatActivity {

    private static ListView list_view_cust_list;
    private static String[] NAMES = {"John", "Mary", "Michel", "Eric Catona", "David Beckam"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        listView();
    }

    public void listView() {
        list_view_cust_list = (ListView) findViewById(R.id.lv_custlist);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.name_list, NAMES);

        //this method set the data to ListView
        list_view_cust_list.setAdapter(adapter);
        list_view_cust_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = (String) list_view_cust_list.getItemAtPosition(position);
                Toast.makeText(CustomerList.this, "position: " + position
                + " value: " + value, Toast.LENGTH_LONG).show();
            }
        });
    }
}
