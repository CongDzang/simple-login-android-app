package com.androiddemo.wavelet.simpleloginapp;

import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class UserInput extends AppCompatActivity {
    private EditText edit_text_name;
    private EditText edit_text_surname;
    private EditText edit_text_marks;
    private Button button_add_data;
    private Button button_view_data;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_input);
        myDb = new DatabaseHelper(this);
        edit_text_name = (EditText) findViewById(R.id.edt_name);
        edit_text_surname = (EditText) findViewById(R.id.edt_sur_name);
        edit_text_marks= (EditText) findViewById(R.id.edt_marks);
        button_add_data = (Button) findViewById(R.id.bt_add_data);
        button_view_data = (Button) findViewById(R.id.bt_show_data);

        addData();
        showAllData();

    }

    public void addData() {
        button_add_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isInserted = myDb.insertData(edit_text_name.getText().toString(),
                        edit_text_surname.getText().toString(),
                        edit_text_marks.getText().toString());

                if(isInserted) {
                    Toast.makeText(UserInput.this, "data inserted", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(UserInput.this, "data not inserted", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void showAllData() {
        button_view_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor res = myDb.getAllData();
                if (res.getCount() == 0) {
                    //Show message here
                    showMessage("Found nothing", "May be there is no any user");
                    return;
                }
                StringBuffer stringBuffer = new StringBuffer();
                while(res.moveToNext()) {
                    stringBuffer.append("Id: " + res.getString(0) + "\n");
                    stringBuffer.append("Name: " + res.getString(1) + "\n");
                    stringBuffer.append("Sur Name: " + res.getString(2) + "\n");
                    stringBuffer.append("Remarks: " + res.getString(3) + "\n");
                }
                //Show all data here
                showMessage("Data:", stringBuffer.toString());
            }
        });
    }

    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();

    }

}
