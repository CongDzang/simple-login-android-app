package com.androiddemo.wavelet.simpleloginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class TimePickerExample extends AppCompatActivity {
    private static TimePicker time_picker;
    private static Button button_display_time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picker_example);
        onClickButtonDisplayTime();
    }

    public void onClickButtonDisplayTime() {
        time_picker = (TimePicker) findViewById(R.id.timePicker);
        button_display_time = (Button) findViewById((R.id.bt_display_time));
        button_display_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), time_picker.getCurrentHour() + " : " +
                        time_picker.getCurrentMinute(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
