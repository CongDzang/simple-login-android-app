package com.androiddemo.wavelet.simpleloginapp;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

public class TimePickerDialogExample extends AppCompatActivity {

    private static Button button_popup_time_picker_dialog;
    static final int TIME_PICKER_DIALOG_ID = 0;
    int hour_x;
    int minute_x;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picker_dialog_example);
        popupTimePickerDialog();
    }

    public void popupTimePickerDialog() {
        button_popup_time_picker_dialog  = (Button) findViewById(R.id.bt_time_picker_dialog);
        button_popup_time_picker_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(TIME_PICKER_DIALOG_ID);
            }
        });
    }
    
    protected TimePickerDialog.OnTimeSetListener kTimePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        hour_x = hourOfDay;
                        minute_x = minute;
                    Toast.makeText(getBaseContext(), hour_x + ":" + minute_x, Toast.LENGTH_LONG);
                }
            };
    //This method create a Dialog - here is time picker dialog
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == TIME_PICKER_DIALOG_ID) {
            return new TimePickerDialog(TimePickerDialogExample.this,
                    kTimePickerListener,
                    hour_x,
                    minute_x,
                    false);
        }
        return null;
    }
}
