package com.androiddemo.wavelet.simpleloginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Login extends AppCompatActivity {

    //Create some control variables
    private static EditText edit_text_username;
    private static EditText edit_text_password;
    private static TextView text_view_attempts;
    private static Button button_login;
    int attempt_counter = 5;
  //  private static TextView

//    DatabaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
 //       myDB = new DatabaseHelper(this);
        login();
    }

    public void login() {

        //Assign control variable to component
        edit_text_username = (EditText) findViewById(R.id.editText);
        edit_text_password = (EditText) findViewById(R.id.editText2);
        text_view_attempts = (TextView) findViewById(R.id.textView5);
        button_login = (Button) findViewById(R.id.button);

        //Set the attempts text view display the attempt counts:
        text_view_attempts.setText(Integer.toString(attempt_counter));

        //onClick Login button event handler
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check if username and password is correct with a constant string or not.
                //If correct change to user activity
                //If not correct reduce the attempt_count, if attempt_count == 0, disable Login button

                if(edit_text_username.getText().toString().equals("admin") && edit_text_password.getText().toString().equals("wavelet")) {
                    Toast.makeText(Login.this, "Username and Password is correct. Welcome!!!", Toast.LENGTH_LONG).show();
                    Intent intent_user = new Intent("com.androiddemo.wavelet.simpleloginapp.UserInput");
                    startActivity(intent_user);
                } else {
                    attempt_counter--;
                    Toast.makeText(Login.this, "Username and Password is not correct. Try again!!!", Toast.LENGTH_LONG).show();
                    text_view_attempts.setText(Integer.toString(attempt_counter));
                    if(attempt_counter == 0) {
                        button_login.setEnabled(false);
                    }
                }
            }
        });

    }

}
