package com.androiddemo.wavelet.simpleloginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class User extends AppCompatActivity {

    private ImageView img_view;
    private Button button_switch_view;
    private Button button_customer_list;

    private int current_image_index;
    int [] images = {R.drawable.fast_food_icon, R.drawable.mango_icon, R.drawable.pear_icon};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        onClickSwitchImageButton();
        onClickCustomerListButton();
    }

    public void onClickSwitchImageButton() {
        img_view = (ImageView) findViewById(R.id.imageView);
        button_switch_view = (Button) findViewById(R.id.btSwitchImage);

        button_switch_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_image_index++;
                current_image_index = current_image_index % images.length;
                img_view.setImageResource(images[current_image_index]);
            }
        });

    }

    public void onClickCustomerListButton() {
        button_customer_list = (Button) findViewById(R.id.bt_custlist);
        button_customer_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_cust_list = new Intent("com.androiddemo.wavelet.simpleloginapp.CustomerList");
                startActivity(intent_cust_list);
            }
        });
    }

}
