package com.androiddemo.wavelet.simpleloginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Seekbar extends AppCompatActivity {

    private static SeekBar seek_bar;
    private static TextView text_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seekbar);
        seekBar();
    }

    public void seekBar() {
        seek_bar = (SeekBar) findViewById(R.id.seekBar);
        text_view  = (TextView) findViewById((R.id.tv_sb));
        text_view.setText("Covered: " + seek_bar.getProgress() + "/" + seek_bar.getMax());

        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress_value;
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value = progress;
                text_view.setText("Covered: " + progress_value + "/" + seek_bar.getMax());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                text_view.setText("Covered: " + progress_value + "/" + seek_bar.getMax());
            }
        });
    }
}
