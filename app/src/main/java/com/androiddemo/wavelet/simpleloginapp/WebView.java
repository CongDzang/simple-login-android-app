package com.androiddemo.wavelet.simpleloginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WebView extends AppCompatActivity {

    private static Button button_display_web;
    private static EditText edit_text_url;
    private static android.webkit.WebView web_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        displayWebView();
    }

    public void displayWebView() {

        button_display_web = (Button) findViewById(R.id.bt_webview);
        edit_text_url = (EditText) findViewById(R.id.edt_url);
        web_view = (android.webkit.WebView) findViewById(R.id.webView);

        button_display_web.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String url = edit_text_url.getText().toString();
                        web_view.getSettings().setLoadsImagesAutomatically(true);
                        web_view.getSettings().setJavaScriptEnabled(true);
                        web_view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        web_view.loadUrl(url);
                    }
                }
        );

    }


}
